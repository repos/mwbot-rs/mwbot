/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use serde::Deserialize;

/// Config file
#[derive(Deserialize, Default)]
pub(crate) struct Config {
    /// The URL of the wiki with script path, e.g. https://wiki.example.org/w/
    #[serde(default)]
    pub(crate) wiki_url: Option<String>,
    #[serde(default)]
    pub(crate) api_url: Option<String>,
    #[serde(default)]
    pub(crate) rest_url: Option<String>,
    pub(crate) auth: Option<Auth>,
    #[serde(default)]
    pub(crate) general: GeneralOptions,
    #[serde(default)]
    pub(crate) edit: EditOptions,
}

#[derive(Deserialize)]
#[serde(untagged)]
pub(crate) enum Auth {
    BotPassword {
        username: String,
        password: String,
    },
    OAuth2 {
        username: String,
        oauth2_token: String,
    },
}

impl Auth {
    pub(crate) fn username(&self) -> &str {
        match &self {
            Auth::BotPassword { username, .. } => username,
            Auth::OAuth2 { username, .. } => username,
        }
    }
}

#[derive(Default, Deserialize)]
pub(crate) struct GeneralOptions {
    pub(crate) maxlag: Option<u32>,
    pub(crate) retry_limit: Option<u32>,
    pub(crate) user_agent: Option<String>,
}

#[derive(Default, Deserialize)]
pub(crate) struct EditOptions {
    pub(crate) mark_as_bot: Option<bool>,
    pub(crate) save_delay: Option<u64>,
    pub(crate) respect_nobots: Option<bool>,
}
