/*
Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::config::{Auth, Config};

/// Build a `mwbot::Bot` instance programmatically
pub struct Builder {
    config: Config,
}

impl Builder {
    pub(crate) fn new(wiki_url: String) -> Self {
        Self {
            config: Config {
                wiki_url: Some(wiki_url),
                auth: None,
                general: Default::default(),
                edit: Default::default(),
                ..Default::default()
            },
        }
    }

    pub(crate) fn new_with_api_url(api_url: String, rest_url: String) -> Self {
        Self {
            config: Config {
                api_url: Some(api_url),
                rest_url: Some(rest_url),
                auth: None,
                general: Default::default(),
                edit: Default::default(),
                ..Default::default()
            },
        }
    }

    pub fn set_botpassword(
        mut self,
        username: String,
        password: String,
    ) -> Self {
        self.config.auth = Some(Auth::BotPassword { username, password });
        self
    }

    pub fn set_oauth2_token(mut self, username: String, token: String) -> Self {
        self.config.auth = Some(Auth::OAuth2 {
            username,
            oauth2_token: token,
        });
        self
    }

    pub fn set_maxlag(mut self, maxlag: u32) -> Self {
        self.config.general.maxlag = Some(maxlag);
        self
    }

    pub fn set_retry_limit(mut self, retry_limit: u32) -> Self {
        self.config.general.retry_limit = Some(retry_limit);
        self
    }

    /// This adds the provided string to the User-agent, it does not
    /// override the default generated one.
    pub fn set_user_agent(mut self, user_agent: String) -> Self {
        self.config.general.user_agent = Some(user_agent);
        self
    }

    pub fn set_mark_as_bot(mut self, mark_as_bot: bool) -> Self {
        self.config.edit.mark_as_bot = Some(mark_as_bot);
        self
    }

    pub fn set_save_delay(mut self, save_delay: u64) -> Self {
        self.config.edit.save_delay = Some(save_delay);
        self
    }

    pub fn set_respect_nobots(mut self, respect_nobots: bool) -> Self {
        self.config.edit.respect_nobots = Some(respect_nobots);
        self
    }

    pub async fn build(self) -> Result<crate::Bot, crate::ConfigError> {
        crate::Bot::from_config(self.config).await
    }
}
