/*
Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::fmt::{Display, Formatter};

#[derive(Clone, Debug)]
#[non_exhaustive]
pub enum UploadWarning {
    Exists,
    WasDeleted,
    Duplicate,
    DuplicateArchive,
    BadFilename,
    Unknown(String),
}

impl UploadWarning {
    pub fn from_key(key: String) -> Self {
        match key.as_str() {
            "exists" => Self::Exists,
            "was-deleted" => Self::WasDeleted,
            "duplicate" => Self::Duplicate,
            "duplicate-archive" => Self::DuplicateArchive,
            "badfilename" => Self::BadFilename,
            _ => Self::Unknown(key),
        }
    }
}

impl Display for UploadWarning {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                UploadWarning::Exists => {
                    "A file with the given name already exists"
                }
                UploadWarning::WasDeleted => {
                    "A file with the given name used to exist but has been deleted"
                }
                UploadWarning::Duplicate => {
                    "The uploaded file exists under a different (or the same) name"
                }
                UploadWarning::DuplicateArchive => {
                    "The uploaded used to exist under a different (or the same) name but has been deleted"
                }
                UploadWarning::BadFilename => {
                    "The file name supplied is not acceptable on this wiki"
                }
                UploadWarning::Unknown(msg) => {
                    msg
                }
            }
        )
    }
}
