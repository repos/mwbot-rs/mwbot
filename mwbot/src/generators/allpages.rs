// SPDX-License-Identifier: GPL-3.0-or-later
//! Generator to get all pages
//!
//! See the [`AllPages`] type documentation for specifics.
use super::{FilterRedirect, Generator};

/// Get all pages in a given namespace
///
/// See [API documentation](https://www.mediawiki.org/wiki/API:Allpages) for more details.
#[derive(Generator)]
#[params(generator = "allpages", gaplimit = "max")]
pub struct AllPages {
    /// The namespace to enumerate
    #[param("gapnamespace")]
    namespace: Option<u32>,
    /// How to filter redirects
    #[param("gapfilterredir")]
    filter_redirect: Option<FilterRedirect>,
    /// Limit to pages with at least this many bytes.
    #[param("gapminsize")]
    min_size: Option<u64>,
    /// Limit to pages with at most this many bytes.
    #[param("gapmaxsize")]
    max_size: Option<u64>,
    /// The page title to start enumerating from.
    #[param("gapfrom")]
    from: Option<String>,
    /// The page title to stop enumerating at.
    #[param("gapto")]
    to: Option<String>,
    /// The title prefix for filtering.
    #[param("gapprefix")]
    prefix: Option<String>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;
    use std::collections::HashMap;
    #[tokio::test]
    async fn test_allpages() {
        let bot = testwp().await;
        let gen = AllPages::new()
            .namespace(0u32)
            .filter_redirect(FilterRedirect::Nonredirects);
        dbg!(gen.params());
        assert_eq!(
            gen.params(),
            HashMap::from([
                ("generator", "allpages".to_string()),
                ("gaplimit", "max".to_string()),
                ("gapnamespace", "0".to_string()),
                ("gapfilterredir", "nonredirects".to_string()),
            ])
        );
        let mut pages = gen.generate(&bot);
        let mut count = 0;
        while let Some(page) = pages.recv().await {
            page.unwrap();
            count += 1;
            if count == 5 {
                break;
            }
        }
        assert_eq!(count, 5);
    }
}
