// SPDX-FileCopyrightText: 2023 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Generators related to unused pages
//!
//! See the [`Unused`] type documentation for specifics.
use super::{Generator, ParamValue};

/// Get all unused categories, images, or templates.
#[derive(Generator)]
#[params(generator = "querypage", gqplimit = "max")]
pub struct Unused {
    #[param("gqppage")]
    target: Target,
    #[param("gqpoffset")]
    offset: Option<u64>,
}

pub enum Target {
    Categories,
    Images,
    Templates,
}

impl ParamValue for Target {
    fn stringify(&self) -> String {
        match self {
            Self::Categories => "Unusedcategories",
            Self::Images => "Unusedimages",
            Self::Templates => "Unusedtemplates",
        }
        .to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::generators::file::FileUsage;
    use crate::tests::testwp;
    use std::collections::HashMap;

    #[tokio::test]
    async fn test_unused_images() {
        let bot = testwp().await;
        let gen = Unused::new(Target::Images);
        dbg!(gen.params());
        assert_eq!(
            gen.params(),
            HashMap::from([
                ("gqplimit", "max".to_string()),
                ("gqppage", "Unusedimages".to_string()),
                ("generator", "querypage".to_string()),
            ])
        );
        let mut pages = gen.generate(&bot);
        let mut count = 0;

        while let Some(page) = pages.recv().await {
            let page = page.unwrap();
            dbg!(page.title());
            assert!(page.is_file());
            assert!(FileUsage::new(vec![page.title().to_string()])
                .generate(&bot)
                .recv()
                .await
                .is_none());

            if count >= 5 {
                break;
            }
            count += 1;
        }
    }
}
