// SPDX-FileCopyrightText: 2023 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Generators related to lint errors
//!
//! These generators require the [Linter](https://www.mediawiki.org/wiki/Extension:Linter)
//! extension to be installed on your wiki.
//!
//! See the [`LintErrors`] type documentation for specifics.
use super::Generator;
use crate::{Bot, Result};
use mwapi_responses::query;

#[query(list = "linterrors")]
pub struct LintErrorResponse {}

/// Get lint errors
///
/// See [API documentation](https://www.mediawiki.org/wiki/Extension:Linter#API) for more details.
#[derive(Generator)]
#[generator(
    return_type = "LintErrorResponseItem",
    response_type = "LintErrorResponse",
    transform_fn = "transform"
)]
#[params(list = "linterrors", lntlimit = "max")]
pub struct LintErrors {
    #[param("lntcategories")]
    categories: Option<Vec<String>>,
    #[param("lntinvisible-categories")]
    invisible_categories: Option<Vec<String>>,
    #[param("lntnamespace")]
    namespaces: Option<Vec<u32>>,
    #[param("lntpageid")]
    page_id: Option<u64>,
    #[param("lntfrom")]
    from: Option<u64>,
}

fn transform(
    _bot: &Bot,
    item: LintErrorResponseItem,
) -> Result<LintErrorResponseItem> {
    Ok(item)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;

    #[tokio::test]
    async fn test_lint_errors() {
        let bot = testwp().await;
        let gen = LintErrors::new();

        let mut lints = gen.generate(&bot);
        let mut count = 0;

        while let Some(lint) = lints.recv().await {
            let lint = lint.unwrap();
            dbg!(lint);

            if count >= 5 {
                break;
            }
            count += 1;
        }
        assert_eq!(count, 5);
    }
}
