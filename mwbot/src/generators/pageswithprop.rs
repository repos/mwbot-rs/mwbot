// SPDX-FileCopyrightText: 2023 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Generators related to page property usage
//!
//! See the [`PagesWithProp`] type documentation for specifics.
use super::{Generator, SortDirection};

/// List all pages using a given page property.
///
/// See [API Documentation](https://www.mediawiki.org/wiki/API:Pageswithprop) for more details.
#[derive(Generator)]
#[params(generator = "pageswithprop", gpwplimit = "max", gpwpprop = "title")]
pub struct PagesWithProp {
    #[param("gpwppropname")]
    prop_name: String,
    #[param("gpwpdir")]
    dir: Option<SortDirection>,
}
#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;

    #[tokio::test]
    async fn test_page_with_props() {
        let bot = testwp().await;

        let gen = PagesWithProp::new("defaultsort");
        let mut pages = gen.generate(&bot);

        let mut count = 0;

        // TODO: We should use filter_parser_function() to check for the presence of defaultsort,
        //       but it's too difficult to assert when defaultsort is specified via transclude
        while let Some(_page) = pages.recv().await {
            if count >= 5 {
                break;
            }
            count += 1;
        }

        assert_eq!(count, 5);
    }
}
