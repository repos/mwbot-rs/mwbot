// SPDX-FileCopyrightText: 2024 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later

#![allow(unused)]

pub use super::transform_to_page;
pub use super::value::ParamValue;
pub use super::{Generator, Params};
pub use crate::error::Error;
pub use crate::page::{InfoResponse, InfoResponseBody, InfoResponseItem, Page};
pub use crate::{Bot, Result};

pub use ::std::clone::Clone;
pub use ::std::collections::HashMap;
pub use ::std::convert::{From, Into};
pub use ::std::default::Default;
pub use ::std::iter::{IntoIterator, Iterator};
pub use ::std::option::Option;

pub mod tokio {
    pub use ::tokio::spawn;
    pub use ::tokio::sync::mpsc::{channel, Receiver};
}

pub mod mwapi_responses {
    pub use ::mwapi_responses::{query_api, ApiResponse};
}
