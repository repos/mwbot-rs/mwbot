// SPDX-FileCopyrightText: 2024 Bingwu Zhang <xtexchooser@duck.com>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Get language links
//!
//! See the [`LangLinks`] type documentation for specifics.

use super::SortDirection;
use crate::generators::Generator;
use crate::{Bot, Result};
use mwapi_responses::query;

#[query(prop = "langlinks", llprop = "langname|url")]
pub struct LangLinksResponse {}

/// Get all language links from the provided pages to other languages.
///
/// See [API documentation](https://www.mediawiki.org/wiki/API:Langlinks) for more details.
#[derive(Default, Debug, Generator)]
#[generator(
    return_type = "LangLinksResponseItem",
    response_type = "LangLinksResponse",
    transform_fn = "transform"
)]
#[params(prop = "langlinks", lllimit = "max")]
pub struct LangLinks {
    #[param("titles")]
    titles: Vec<String>,
    #[param("lllang")]
    lang: Option<String>,
    #[param("lltitle")]
    lang_title: Option<String>,
    #[param("lldir")]
    sort: Option<SortDirection>,
    #[param("llinlanguagecode")]
    lang_name_locale: Option<String>,
}

fn transform(
    _bot: &Bot,
    item: LangLinksResponseItem,
) -> Result<LangLinksResponseItem> {
    // do nothing
    Ok(item)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;

    #[tokio::test]
    async fn test_lang_links() {
        let bot = testwp().await;
        let gen = LangLinks::new(vec!["Mwbot-rs/Langlink".to_string()]);
        dbg!(gen.params());

        let mut links = gen.generate(&bot);
        let mut found = Vec::new();

        let langlinks = links.recv().await.unwrap().unwrap().langlinks;
        for langlink in langlinks {
            found.push((langlink.lang.to_string(), langlink.title.to_string()));
        }
        assert!(links.recv().await.is_none());

        assert_eq!(found, [("en".to_string(), "Stick style".to_string())])
    }
}
