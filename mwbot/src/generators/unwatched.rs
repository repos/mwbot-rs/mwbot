// SPDX-FileCopyrightText: 2023 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Generator for unwatched pages
//!
//! See the [`UnwatchedPages`] type documentation for specifics.
use super::Generator;

#[derive(Generator)]
#[params(generator = "querypage", gqplimit = "max", gqppage = "Unwatchedpages")]
pub struct UnwatchedPages {
    #[param("gqpoffset")]
    offset: Option<u64>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::{has_userright, is_authenticated, testwp};
    use std::collections::HashMap;

    #[tokio::test]
    async fn test_unwatched_pages() {
        let bot = testwp().await;
        // Anonymous users cannot use the watch list
        if !is_authenticated() || !has_userright(&bot, "unwatchedpages").await {
            return;
        }
        let gen = UnwatchedPages::new();
        dbg!(gen.params());
        assert_eq!(
            gen.params(),
            HashMap::from([
                ("generator", "querypage".to_string()),
                ("gqppage", "Unwatchedpages".to_string()),
                ("gqplimit", "max".to_string()),
            ]),
        );

        let mut pages = gen.generate(&bot);
        let mut count = 0;

        while let Some(page) = pages.recv().await {
            let page = page.unwrap();
            dbg!(page.title());

            if count >= 5 {
                break;
            }
            count += 1;
        }
        assert_eq!(count, 5);
    }
}
