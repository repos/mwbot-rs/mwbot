// SPDX-License-Identifier: GPL-3.0-or-later
//! Generator to search for pages
//!
//! See the [`Search`] type documentation for specifics.
use super::{Generator, ParamValue};

/// Search for pages
///
/// See [API documentation](https://www.mediawiki.org/wiki/API:Search) for more details.
#[derive(Generator)]
#[params(generator = "search", gsrlimit = "max")]
pub struct Search {
    #[param("gsrsearch")]
    search: String,
    #[param("gsrnamespace")]
    namespace: Option<Vec<u32>>,
    #[param("gsroffset")]
    offset: Option<u64>,
    #[param("gsrqiprofile")]
    qi_profile: Option<String>,
    #[param("gsrwhat")]
    what: Option<SearchWhat>,
    #[param("gsrinterwiki")]
    interwiki: Option<bool>,
    #[param("gsrenablerewrites")]
    enable_rewrites: Option<bool>,
}

pub enum SearchWhat {
    Nearmatch,
    Text,
    Title,
}

impl ParamValue for SearchWhat {
    fn stringify(&self) -> String {
        match self {
            Self::Nearmatch => "nearmatch",
            Self::Text => "text",
            Self::Title => "title",
        }
        .to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_generator_boolean() {
        let gen = Search::new("foo".to_string()).interwiki(true);
        assert!(gen.params().contains_key("gsrinterwiki"));
        let gen = gen.interwiki(false);
        assert!(!gen.params().contains_key("gsrinterwiki"));
    }
}
