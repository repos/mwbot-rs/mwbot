// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>

use mwtimestamp::Timestamp;

/// Trait to represent a value that can be an API parameter value.
///
/// A blanket implementation for `Vec<T: ParamValue>` is provided to
/// handle [mutlivalue](https://www.mediawiki.org/w/api.php#main/datatypes) parameters.
///
/// It is roughly the same as the standard library's [`ToString`] trait.
pub trait ParamValue {
    /// Serialize the value into a string for the API
    fn stringify(&self) -> String;
}

macro_rules! impl_value {
    ( $x:ty ) => {
        impl ParamValue for $x {
            fn stringify(&self) -> String {
                self.to_string()
            }
        }
    };
}

impl_value!(u32);
impl_value!(u64);
impl_value!(String);
impl_value!(Timestamp);

impl<T: ParamValue> ParamValue for Vec<T> {
    fn stringify(&self) -> String {
        let x: Vec<_> = self.iter().map(|i| i.stringify()).collect();
        x.join("|")
    }
}
