// SPDX-FileCopyrightText: 2023 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Generator for lonely pages (not linked by other pages)
//!
//! See the [`LonelyPages`] type documentation for specifics.
use super::Generator;

#[derive(Generator)]
#[params(generator = "querypage", gqplimit = "max", gqppage = "Lonelypages")]
pub struct LonelyPages {
    #[param("gqpoffset")]
    offset: Option<u64>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::generators::link::LinksHere;
    use crate::tests::testwp;
    use std::collections::HashMap;

    #[tokio::test]
    async fn test_lonely_pages() {
        let bot = testwp().await;
        let gen = LonelyPages::new();
        dbg!(gen.params());
        assert_eq!(
            gen.params(),
            HashMap::from([
                ("generator", "querypage".to_string()),
                ("gqppage", "Lonelypages".to_string()),
                ("gqplimit", "max".to_string()),
            ]),
        );
        let mut pages = gen.generate(&bot);
        let mut count = 0;

        while let Some(page) = pages.recv().await {
            let page = page.unwrap();
            dbg!(page.title());
            assert!(LinksHere::new(vec![page.title().to_string()])
                .generate(&bot)
                .recv()
                .await
                .is_none());

            if count >= 5 {
                break;
            }
            count += 1;
        }
    }
}
