use super::{Generator, SortDirection};

/// Get all templates that is transcluded in the given pages.
///
/// See [API documentation](https://www.mediawiki.org/wiki/API:Templates) for more details.
#[derive(Generator)]
#[params(generator = "templates", gtllimit = "max")]
pub struct Templates {
    #[param("titles")]
    titles: Vec<String>,
    #[param("gtlnamespace")]
    namespaces: Option<Vec<u32>>,
    #[param("gtltemplates")]
    templates: Option<Vec<String>>,
    #[param("gtldir")]
    sort: Option<SortDirection>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;

    #[tokio::test]
    async fn test_templates() {
        let bot = testwp().await;
        let mut gen = Templates::new(vec!["Mwbot-rs/Transcluded".to_string()])
            .generate(&bot);

        let mut found = Vec::new();

        while let Some(page) = gen.recv().await {
            let page = page.unwrap();
            dbg!(page.title());

            found.push(page.title().to_string());
        }

        assert!(found.contains(&"Main Page".to_string()));
    }
}
