// SPDX-FileCopyrightText: 2023 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Generator for QueryPage-based special pages
//!
//! See the [`QueryPage`] type documentation for specifics.
use super::Generator;

/// Get a list provided by a QueryPage-based special page.
///
/// See [API documentation](https://www.mediawiki.org/wiki/API:Querypage) for more details.
#[derive(Generator)]
#[params(generator = "querypage", gqplimit = "max")]
pub struct QueryPage {
    #[param("gqppage")]
    special_page: String,
    #[param("gqpoffset")]
    offset: Option<u64>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;

    #[tokio::test]
    async fn test_list_duplicated_files() {
        let bot = testwp().await;
        let gen = QueryPage::new("ListDuplicatedFiles");

        let mut pages = gen.generate(&bot);
        let mut count = 0;

        while let Some(page) = pages.recv().await {
            let page = page.unwrap();
            dbg!(page.title());

            if count >= 5 {
                break;
            }
            count += 1;
        }
        assert_eq!(count, 5);
    }
}
