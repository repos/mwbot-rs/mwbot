// SPDX-FileCopyrightText: 2023 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Generator to get a random set of pages
//!
//! See the [`RandomPages`] type documentation for specifics.
use super::{Generator, ParamValue};

/// Get pages with random starting point.
///
/// See [API documentation](https://www.mediawiki.org/wiki/API:Random) for more details.
#[derive(Generator)]
#[params(generator = "random", grnlimit = "max")]
pub struct RandomPages {
    #[param("grnnamespace")]
    namespaces: Option<Vec<u32>>,
    #[param("grnfilterredir")]
    filterredir: Option<FilterRedirect>,
}

pub enum FilterRedirect {
    All,
    NonRedirects,
    Redirects,
}

impl ParamValue for FilterRedirect {
    fn stringify(&self) -> String {
        match self {
            Self::All => "all",
            Self::NonRedirects => "nonredirects",
            Self::Redirects => "redirects",
        }
        .to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;

    #[tokio::test]
    async fn test_random_pages() {
        let bot = testwp().await;
        let gen = RandomPages::new();

        let mut pages = gen.generate(&bot);
        let mut count = 0;

        while let Some(page) = pages.recv().await {
            let _page = page.unwrap();

            if count >= 5 {
                break;
            }
            count += 1;
        }

        assert_eq!(count, 5);
    }
}
