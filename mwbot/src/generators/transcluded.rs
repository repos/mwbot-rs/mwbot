// SPDX-FileCopyrightText: 2024 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Generator to get transcluded pages
//!
//! See the [`TranscludedIn`] type documentation for specifics.

use super::{Generator, ParamValue};

/// Get all pages that transclude the given pages.
///
/// See [API documentation](https://www.mediawiki.org/wiki/API:Transcludedin) for more details.
#[derive(Generator)]
#[params(generator = "transcludedin", gtilimit = "max")]
pub struct TranscludedIn {
    #[param("titles")]
    titles: Vec<String>,
    #[param("gtinamespace")]
    namespaces: Option<Vec<u32>>,
    #[param("gtishow")]
    filter: Option<Filter>,
}

pub enum Filter {
    Redirect,
    NonRedirect,
}

impl ParamValue for Filter {
    fn stringify(&self) -> String {
        match self {
            Self::Redirect => "redirect",
            Self::NonRedirect => "!redirect",
        }
        .to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;

    #[tokio::test]
    async fn test_transcluded_in() {
        let bot = testwp().await;
        let mut gen =
            TranscludedIn::new(vec!["Main Page".to_string()]).generate(&bot);

        let mut found = Vec::new();

        while let Some(page) = gen.recv().await {
            let page = page.unwrap();
            dbg!(page.title());

            found.push(page.title().to_string());
        }

        assert!(found.contains(&"Mwbot-rs/Transcluded".to_string()));
    }
}
