// SPDX-FileCopyrightText: 2023 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Generators related to uncategorized pages
//!
//! See the [`Uncategorized`] type documentation for specifics.
use super::{Generator, ParamValue};

/// Get all uncategorized categories, images, pages, or templates.
#[derive(Generator)]
#[params(generator = "querypage", gqplimit = "max")]
pub struct Uncategorized {
    #[param("gqppage")]
    target: Target,
    #[param("gqpoffset")]
    offset: Option<u64>,
}

pub enum Target {
    Categories,
    Images,
    Pages,
    Templates,
}

impl ParamValue for Target {
    fn stringify(&self) -> String {
        match self {
            Self::Categories => "Uncategorizedcategories",
            Self::Images => "Uncategorizedimages",
            Self::Pages => "Uncategorizedpages",
            Self::Templates => "Uncategorizedtemplates",
        }
        .to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;
    use parsoid::WikinodeIterator as _;
    use std::collections::HashMap;

    #[tokio::test]
    async fn test_uncategorized_pages() {
        let bot = testwp().await;
        let gen = Uncategorized::new(Target::Pages);
        dbg!(gen.params());
        assert_eq!(
            gen.params(),
            HashMap::from([
                ("generator", "querypage".to_string()),
                ("gqppage", "Uncategorizedpages".to_string()),
                ("gqplimit", "max".to_string()),
            ])
        );
        let mut pages = gen.generate(&bot);
        let mut count = 0;

        while let Some(page) = pages.recv().await {
            let page = page.unwrap();
            if !page.exists().await.unwrap() {
                continue;
            }
            dbg!(page.title());
            let html = page.html().await.unwrap().into_mutable();
            assert!(html.filter_categories().is_empty());

            if count >= 5 {
                break;
            }
            count += 1;
        }
    }
}
