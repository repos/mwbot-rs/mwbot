// SPDX-FileCopyrightText: 2024 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later
use mwapi_responses::query;
use mwbot::generators::Generator;
use mwbot::{Bot, Result};

#[query(prop = "info|categories", inprop = "associatedpage|url")]
struct MyPageInfoResponse;

#[derive(Generator)]
#[generator(
    crate = "mwbot",
    return_type = "MyPageInfoResponseItem",
    response_type = "MyPageInfoResponse",
    transform_fn = "noop_transform"
)]
#[params(generator = "allpages", gaplimit = "max")]
#[allow(unused)]
struct MyAllPageGenerator {}

#[allow(unused)]
fn noop_transform(
    _bot: &Bot,
    item: MyPageInfoResponseItem,
) -> Result<MyPageInfoResponseItem> {
    Ok(item)
}

#[test]
fn test() {
    //
}
