use mwbot::generators::Generator;

#[derive(Generator)]
#[params(dummy = "foo")]
struct MissingGenerator {}

fn main() {}
