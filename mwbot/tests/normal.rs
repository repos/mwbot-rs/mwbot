// SPDX-FileCopyrightText: 2024 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later
use mwbot::generators::Generator;

#[derive(Generator)]
#[generator(crate = "mwbot")]
#[params(generator = "categorymembers", gcmlimit = "max")]
pub struct CategoryMembers {
    #[param("gcmtitle")]
    title: String,
    #[param("gcmnamespace")]
    namespace: Option<Vec<u32>>,
}

#[test]
fn test() {}
