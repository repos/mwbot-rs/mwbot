## 0.5.0-rc.2 / 2024-11-23
* Raise MSRV to 1.74.
* Support `list=globalblocks`.
* [BREAKING] Deserialize free-form `params` field in `list=logevents` as a `serde_json::value::Value`.
* Fix simple upload to parse upload response properly.
* Add support for `actions` field in `PartialBlockRestrictions`.

## 0.5.0-rc.1 / 2024-06-02
* [BREAKING] Upgrade `mwapi` to `0.7`. The feature to enable it named `mwapi-07`.
* Support `list=recentchanges`, `prop=langlinks` and `prop=categoryinfo`.

## 0.4.2 / 2024-01-23
* Fix ContinueVisitor to handle linterrors `continue_` block ([T354504](https://phabricator.wikimedia.org/T354504)).

## 0.4.1 / 2023-12-21
* Fix errors with finding `mwtimestamp` crate in the macro.
* Auto-generate crate readme with `cargo-readme`.

## 0.4.0 / 2023-12-14
* Increase MSRV to 1.67.
* Add `mwapi_responses::query_api` function to easily make requests with the `mwapi` crate.
  This is behind the `mwapi-06` feature.
* Support queries where the `continue` value is a number.
* Use `Timestamp` and `Expiry` from new `mwtimestamp` crate.
* Add support for `list=usercontribs` and  `list=linterrors`
* Fix `list=logevents` for some log types

## 0.3.4 / 2023-07-15
* Increase MSRV to 1.65.
* Return proper compiler errors instead of panicking on bad input.
* Handle the `query` field not existing in some responses ([T338102](https://phabricator.wikimedia.org/T338102)).

## 0.3.3 / 2023-03-19
* Add `partial` flag for `list=blocks`

## 0.3.2 / 2023-03-11
* Add support for `list=blocks`
* Increase MSRV to 1.63.

## 0.3.1 / 2022-10-01
* Git repository moved to [Wikimedia GitLab](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot).
* Issue tracking moved to [Wikimedia Phabricator](https://phabricator.wikimedia.org/project/profile/6182/).

## 0.3.0 / 2022-09-19
* Add support for `prop=pageassessments`
* Increase MSRV to 1.60.

## 0.3.0-alpha.4 / 2022-09-11
* Add preliminary support for `prop=imageinfo`
* Bump MSRV to 1.59

## 0.3.0-alpha.3 / 2022-05-01
* [BREAKING] Use `u64` for all revision ID fields. Fixes [#56](https://gitlab.com/mwbot-rs/mwbot/-/issues/56).
* Fix fieldname of `list=categories`
* Add support for `prop=links`

## 0.3.0-alpha.2 / 2022-01-04
* Add Display impl for Timestamp and Expiry.
* Make it easier to process normalized/redirected titles by adding a
  `title_map()` helper function.
* Queries like `action=query&titles=[...]` are now supported. This means
  that a plain `#[query()]` compiles.
* A default-on `derive` feature can be disabled to remove the dependency on
  the underlying proc-macro crate for cases where it isn't needed.

## 0.3.0-alpha.1 / 2021-12-24
* [BREAKING] `ApiResponse::items()` now explicitly returns an iterator instead of a slice.
* [BREAKING] Drop `get_continue()` and `has_continue()` functions, just use the
  `continue_` property directly.
* [BREAKING] The generated struct now uses the same visibility of the original
  struct. Fixes [#4](https://gitlab.com/mwbot-rs/mwbot/-/issues/4).
* Fields that represent a timestamp are now deserialized to a `Timestamp` type
  that wraps around the [`time`](https://docs.rs/time) crate's
  `OffsetDateTime` (UTC).
* Fields that represent an expiry are now deserialized to a `Expiry` enum that
  is either infinity or a `Timestamp`.
* Add `ApiResponse::into_items()` to iterate over owned items.
* `prop=info`'s notificationtimestamp field is now correctly deserialized to
  `Option<Timestamp>` instead of an empty string for no value.

## 0.2.2 / 2021-11-14
* Add support for `list=embeddedin`

## 0.2.1 / 2021-11-07
* Add missing `source` field to ProtectionInfo
* Add support for `&redirects=1`
* Support responses with invalid titles

## 0.2.0 / 2021-11-01
* [BREAKING] Require Rust 1.56
* [BREAKING] Refocus on only supporting `action=query`.
* Add support and metadata for:
  * `list=categorymembers`
  * `prop=categories`
  * `prop=info`
  * `prop=revisions`
* Add `Response::items()` to allow easily iterating over what we mostly
  care about.
* Invalidate build cache when metadata changes.
* [BREAKING] Drop `Response::from_value()` helper, use
  `serde_json::from_value()` directly instead.

## 0.1.0 / 2021-10-24
* Initial release
