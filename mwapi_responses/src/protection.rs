/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use mwtimestamp::Expiry;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct ProtectionInfo {
    #[serde(rename = "type")]
    pub type_: String,
    pub level: String,
    pub expiry: Expiry,
    #[serde(default)]
    pub cascade: bool,
    /// Source the cascade protection is coming from
    pub source: Option<String>,
}
