/*
Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
use serde::{de, Deserialize};

#[derive(Clone, Debug, Deserialize)]
pub struct PageAssessment {
    pub class: String,
    pub importance: String,
}

/// Not public, for internal use only.
///
/// Deserialize booleans, treating empty string as `true`.
///
/// During the switch to API formatversion=2, some API modules continued to
/// output empty strings for booleans, so this gracefully handles that case
/// as well as when they are fixed to return proper booleans.
#[doc(hidden)]
pub fn deserialize_bool_or_string<'de, D>(
    deserializer: D,
) -> Result<bool, D::Error>
where
    D: de::Deserializer<'de>,
{
    let val = BooleanCompat::deserialize(deserializer)?;
    match val {
        BooleanCompat::V2(bool) => Ok(bool),
        // Empty string is true, anything else is an error.
        BooleanCompat::V1(val) => match val.as_str() {
            "" => Ok(true),
            _ => Err(de::Error::custom(format!("Unexpected value: {}", val))),
        },
    }
}

/// Enum to represent booleans in both `formatversion=1` (present empty string
/// for true) and `formatversion=2` (real booleans).
#[derive(Clone, Debug, Deserialize)]
#[serde(untagged)]
enum BooleanCompat {
    V2(bool),
    V1(String),
}
