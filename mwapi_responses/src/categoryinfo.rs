// SPDX-FileCopyrightText: 2024 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later

use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct CategoryInfo {
    pub size: u32,
    pub pages: u32,
    pub files: u32,
    pub subcats: u32,
    pub hidden: bool,
}
