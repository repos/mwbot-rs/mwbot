/*
 * SPDX-FileCopyrightText: 2023 Misato Kano <me@mirror-kt.dev>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use serde::de::{MapAccess, Visitor};
use serde::{Deserialize, Deserializer};
use std::collections::HashMap;
use std::fmt::Formatter;

#[derive(Deserialize)]
#[serde(untagged)]
enum StringyNumber {
    String(String),
    Number(i64),
}

impl StringyNumber {
    fn stringify(self) -> String {
        match self {
            Self::String(string) => string,
            Self::Number(num) => num.to_string(),
        }
    }
}

/// The `API:Querypage` generator fails when trying to deserialize it as a string because
/// the return value of continue contains a number.
/// This deserializer also treats numbers as strings and deserializes them.
pub fn deserialize_continue<'de, D>(
    deserializer: D,
) -> Result<HashMap<String, String>, D::Error>
where
    D: Deserializer<'de>,
{
    struct ContinueVisitor;

    impl<'de> Visitor<'de> for ContinueVisitor {
        type Value = HashMap<String, String>;

        fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
            formatter.write_str("'continue_' must be a string")
        }

        fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error>
        where
            A: MapAccess<'de>,
        {
            let mut values = HashMap::new();
            while let Some((key, value)) =
                map.next_entry::<String, StringyNumber>()?
            {
                values.insert(key, value.stringify());
            }

            Ok(values)
        }
    }

    let visitor = ContinueVisitor;
    deserializer.deserialize_map(visitor)
}
