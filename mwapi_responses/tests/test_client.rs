pub async fn test<T: serde::de::DeserializeOwned>(
    params: &[(&str, &str)],
) -> anyhow::Result<T> {
    eprintln!("params: {}", serde_urlencoded::to_string(params)?);
    let client = reqwest::Client::builder()
        .user_agent("mwapi_responses testing")
        .build()?;
    let resp = client
        .get("https://en.wikipedia.org/w/api.php")
        .query(params)
        .send()
        .await?
        .text()
        .await?;
    eprintln!("response: {}", &resp);
    Ok(serde_json::from_str(&resp)?)
}
