## 0.3.1-rc.2 / 2024-06-13
* Improve Cargo.toml metadata.

## 0.3.0-rc.1 / 2024-06-06
* [BREAKING] Depend on `parsoid` 0.10.
* Make `PROSE_SELECTOR` and `remove_noncounted_elements()` public.
* Increase MSRV to 1.74.

## 0.2.0 / 2023-12-15
* Increase MSRV to 1.67.
* [BREAKING] Depend on `parsoid` 0.9.0.
* Implement serde's `Serialize` and `Deserialize` traits on `ProseSize`.
  This requires the `serde-1` feature to be enabled.

## 0.1.0 / 2023-04-02
* Increase MSRV to 1.63.

## 0.1.0-alpha.3 / 2023-02-20
* Only count the main references group towards the references size.
* Allow passing mutable `Wikicode` instances, with a warning in documentation.
* `parsoid_stylesheet()` returns CSS that highlights, as close as possible,
  elements which are counted towards prosesize.

## 0.1.0-alpha.2 / 2023-02-15
* Don't incorrectly count `<math>` tags.
* Use broader method of finding references.
* Internal improvements for optimization.

## 0.1.0-alpha.1 / 2023-02-14
* Initial release.
