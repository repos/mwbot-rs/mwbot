## 0.1.2 / 2025-01-25
* Raise MSRV to 1.74.
* Use docsrs cfg option
* Update various dependencies (non-breaking).

## 0.1.1 / 2024-06-02
* Raise MSRV to 1.73.
* Serialize to microseconds, not nanoseconds
* Add `Timestamp::now()` for the current time in UTC.

## 0.1.0 / 2023-11-26
* Initial release, split from `mwapi_responses`.
