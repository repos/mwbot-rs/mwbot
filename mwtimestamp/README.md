# mwtimestamp

[![crates.io](https://img.shields.io/crates/v/mwtimestamp.svg)](https://crates.io/crates/mwtimestamp)
[![docs.rs](https://img.shields.io/docsrs/mwtimestamp?label=docs.rs)](https://docs.rs/mwtimestamp)
[![docs (main)](https://img.shields.io/badge/doc.wikimedia.org-green?label=docs%40main)](https://doc.wikimedia.org/mwbot-rs/mwbot/mwtimestamp/)
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/commits/main)
[![coverage report](https://img.shields.io/endpoint?url=https%3A%2F%2Fdoc.wikimedia.org%2Fcover%2Fmwbot-rs%2Fmwbot%2Fcoverage%2Fcoverage.json)](https://doc.wikimedia.org/cover/mwbot-rs/mwbot/coverage)

`mwtimestamp` is a library for parsing and formatting MediaWiki
timestamps, powered by [`chrono`](https://docs.rs/chrono).

The [MediaWiki API](https://www.mediawiki.org/w/api.php#main/datatype/timestamp)
typically produces ISO 8601 timestamps. In some cases, like protection or
block expiry, it may alternatively return the string "infinity" to represent
that there is no end period.

```rust
use mwtimestamp::{Expiry, Timestamp};
// Deserializing a fixed timestamp
let finite: Timestamp = serde_json::from_str("\"2001-01-15T14:56:00Z\"").unwrap();
assert_eq!(
    finite.date_naive(),
    chrono::NaiveDate::from_ymd_opt(2001, 1, 15).unwrap(),
);
// Deserializing an infinite timestamp
let infinity: Expiry = serde_json::from_str("\"infinity\"").unwrap();
assert!(infinity.is_infinity());
```

### Contributing
`mwtimestamp` is a part of the [`mwbot-rs` project](https://www.mediawiki.org/wiki/Mwbot-rs).
We're always looking for new contributors, please [reach out](https://www.mediawiki.org/wiki/Mwbot-rs#Contributing)
if you're interested!

## License
This crate is released under GPL-3.0-or-later.
See [COPYING](./COPYING) for details.
