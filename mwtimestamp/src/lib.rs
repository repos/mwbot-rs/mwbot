/*
 * SPDX-FileCopyrightText: 2023 Misato Kano <me@mirror-kt.dev>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
//! `mwtimestamp` is a library for parsing and formatting MediaWiki
//! timestamps, powered by [`chrono`](https://docs.rs/chrono).
//!
//! The [MediaWiki API](https://www.mediawiki.org/w/api.php#main/datatype/timestamp)
//! typically produces ISO 8601 timestamps. In some cases, like protection or
//! block expiry, it may alternatively return the string "infinity" to represent
//! that there is no end period.
//!
//! ```
//! use mwtimestamp::{Expiry, Timestamp};
//! // Deserializing a fixed timestamp
//! let finite: Timestamp = serde_json::from_str("\"2001-01-15T14:56:00Z\"").unwrap();
//! assert_eq!(
//!     finite.date_naive(),
//!     chrono::NaiveDate::from_ymd_opt(2001, 1, 15).unwrap(),
//! );
//! // Deserializing an infinite timestamp
//! let infinity: Expiry = serde_json::from_str("\"infinity\"").unwrap();
//! assert!(infinity.is_infinity());
//! ```
//!
//! ## Contributing
//! `mwtimestamp` is a part of the [`mwbot-rs` project](https://www.mediawiki.org/wiki/Mwbot-rs).
//! We're always looking for new contributors, please [reach out](https://www.mediawiki.org/wiki/Mwbot-rs#Contributing)
//! if you're interested!
#![deny(clippy::all)]
#![deny(rustdoc::all)]
#![cfg_attr(docsrs, feature(doc_cfg))]

use chrono::{DateTime, TimeZone, Utc};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::ops::Deref;

/// Represents a MediaWiki timestamp, which are always in UTC.
/// It is basically a wrapper around [`DateTime`] but serializes and
/// deserializes in the ISO 8601 format MediaWiki wants.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Deserialize)]
pub struct Timestamp(DateTime<Utc>);

impl Timestamp {
    pub fn new<Tz: TimeZone>(date_time: DateTime<Tz>) -> Self {
        let utc_date_time = date_time.naive_utc().and_utc();
        Self(utc_date_time)
    }

    pub fn now() -> Self {
        Self(Utc::now())
    }
}

impl<Tz: TimeZone> From<DateTime<Tz>> for Timestamp {
    fn from(value: DateTime<Tz>) -> Self {
        Self::new(value)
    }
}

impl Deref for Timestamp {
    type Target = DateTime<Utc>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl fmt::Display for Timestamp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            self.0.to_rfc3339_opts(chrono::SecondsFormat::Micros, true)
        )
    }
}

impl Serialize for Timestamp {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.collect_str(self)
    }
}

/// A MediaWiki expiry, which can either be infinity (aka indefinite) or
/// a specific [`Timestamp`].
#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum Expiry {
    #[serde(rename = "infinity")]
    Infinity,
    #[serde(untagged)]
    Finite(Timestamp),
}

impl Expiry {
    /// Whether the expiry is for an infinite (aka indefinite) amount of time
    pub fn is_infinity(&self) -> bool {
        matches!(self, Self::Infinity)
    }

    /// If the expiry is finite, get a timestamp for it
    pub fn as_timestamp(&self) -> Option<&Timestamp> {
        match self {
            Expiry::Infinity => None,
            Expiry::Finite(datetime) => Some(datetime),
        }
    }
}

impl fmt::Display for Expiry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Expiry::Infinity => {
                write!(f, "infinity")
            }
            Expiry::Finite(ts) => {
                write!(f, "{ts}")
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

    #[test]
    fn test_timestamp_deserialize() {
        let ts: Timestamp =
            serde_json::from_str("\"2001-01-15T14:56:00.000000Z\"").unwrap();
        assert_eq!(
            ts.date_naive(),
            NaiveDate::from_ymd_opt(2001, 1, 15).unwrap(),
        );
        assert_eq!(ts.time(), NaiveTime::from_hms_opt(14, 56, 00).unwrap(),);
    }

    #[test]
    fn test_timestamp_serialize() {
        let ts = Timestamp(
            NaiveDateTime::new(
                NaiveDate::from_ymd_opt(2001, 1, 15).unwrap(),
                NaiveTime::from_hms_opt(14, 56, 00).unwrap(),
            )
            .and_utc(),
        );
        assert_eq!(
            serde_json::to_string(&ts).unwrap(),
            "\"2001-01-15T14:56:00.000000Z\""
        );
        assert_eq!(ts.to_string(), "2001-01-15T14:56:00.000000Z");
    }

    #[test]
    fn test_expiry_deserialize() {
        let infinity: Expiry = serde_json::from_str("\"infinity\"").unwrap();
        assert!(infinity.as_timestamp().is_none());
        assert!(infinity.is_infinity());

        let finite: Expiry =
            serde_json::from_str("\"2001-01-15T14:56:00.000000Z\"").unwrap();
        assert_eq!(
            finite.as_timestamp().unwrap().date_naive(),
            NaiveDate::from_ymd_opt(2001, 1, 15).unwrap(),
        );
        assert_eq!(
            finite.as_timestamp().unwrap().time(),
            NaiveTime::from_hms_opt(14, 56, 00).unwrap(),
        );
    }

    #[test]
    fn test_expiry_serialize() {
        assert_eq!(
            serde_json::to_string(&Expiry::Infinity).unwrap(),
            "\"infinity\""
        );
        let finite = Expiry::Finite(Timestamp(
            NaiveDateTime::new(
                NaiveDate::from_ymd_opt(2001, 1, 15).unwrap(),
                NaiveTime::from_hms_opt(14, 56, 00).unwrap(),
            )
            .and_utc(),
        ));
        assert_eq!(
            serde_json::to_string(&finite).unwrap(),
            "\"2001-01-15T14:56:00.000000Z\""
        );
    }
}
