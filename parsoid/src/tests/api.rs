/*
Copyright (C) 2020-2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::tests::test_client::testwp_client;
use crate::{Error, Result};

#[tokio::test]
async fn test_get() -> Result<()> {
    let client = testwp_client();
    let html = client.get_raw("Main Page").await?;
    assert!(html.contains("wmf-config/InitialiseSettings.php"));
    match client.get("ThisPageDoesNotExist").await {
        Err(Error::PageDoesNotExist(title)) => {
            assert_eq!("ThisPageDoesNotExist".to_string(), title);
        }
        _ => {
            panic!("Test did not fail with Error::PageDoesNotExist()");
        }
    }
    Ok(())
}

#[tokio::test]
async fn test_get_revision() -> Result<()> {
    let client = testwp_client();
    let code = client.get_revision("Main Page", 1).await?.into_mutable();
    assert!(code.text_contents().contains(
        "This subdomain is reserved for the creation of a Wikipedia"
    ));
    assert_eq!(code.revision_id(), Some(1));
    Ok(())
}

#[tokio::test]
async fn test_get_redirect() -> Result<()> {
    let client = testwp_client();
    let code = client.get("Mwbot-rs/Redirect").await?.into_mutable();
    let redirect = code.redirect();
    assert!(redirect.is_some());
    assert_eq!(&redirect.unwrap().target(), "Main Page");
    Ok(())
}

#[tokio::test]
async fn test_transform_to_html() -> Result<()> {
    let client = testwp_client();
    let html = client
        .transform_to_html_raw("{{1x|This is HTML now}}")
        .await?;
    assert!(html.contains("This is HTML now"));
    Ok(())
}

#[tokio::test]
async fn test_transform_to_wikitext() -> Result<()> {
    let client = testwp_client();
    let wikitext = client
        .transform_to_wikitext_raw(
            "<a rel=\"mw:WikiLink\" href=\"./Foo\">Foo bar</a>",
            None,
            None,
            None,
        )
        .await?;
    assert_eq!(wikitext, "[[Foo|Foo bar]]".to_string());
    Ok(())
}

#[tokio::test]
async fn test_immutable() -> Result<()> {
    let client = testwp_client();
    let code = client.get("Main Page").await?;
    let wikitext = client.transform_to_wikitext(&code).await?;
    assert!(wikitext.contains("Wikipedia"));
    Ok(())
}
