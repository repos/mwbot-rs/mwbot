// SPDX-FileCopyrightText: 2024 Kunal Mehta <legoktm@debian.org>
// SPDX-License-Identifier: GPL-3.0-or-later
use crate::prelude::*;
use crate::tests::test_client;
use crate::Result;

#[tokio::test]
async fn test_indicator() -> Result<()> {
    let client = test_client::testwp_client();
    let code = client
        .transform_to_html(
            r#"<indicator name="test">[[Some wikitext]]</indicator>"#,
        )
        .await?
        .into_mutable();
    dbg!(code.to_string());
    let indicators: Vec<_> = code
        .inclusive_descendants()
        .filter_map(|node| node.as_indicator())
        .collect();
    dbg!(&indicators);
    assert_eq!(indicators.len(), 1);
    let indicator = &indicators[0];
    assert_eq!(&indicator.name()?, "test");
    assert_eq!(&indicator.wikitext()?, "[[Some wikitext]]");
    indicator.set_name("new name")?;
    indicator.set_wikitext("[[A new link]]")?;
    let wikitext = client.transform_to_wikitext(&code).await?;
    assert_eq!(
        &wikitext,
        "<indicator name=\"new name\">[[A new link]]</indicator>"
    );
    Ok(())
}

#[tokio::test]
async fn test_new_indicator() -> Result<()> {
    let client = test_client::testwp_client();
    let code = Wikicode::new("");
    code.prepend(&Indicator::new("test", "[[wikitext]]")?);
    let wikitext = client.transform_to_wikitext(&code).await?;
    assert_eq!(
        wikitext,
        "<indicator name=\"test\">[[wikitext]]</indicator>"
    );
    Ok(())
}
