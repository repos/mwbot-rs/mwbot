/*
Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::image::HorizontalAlignment;
use crate::prelude::*;
use crate::tests::test_client;
use anyhow::Result;

#[tokio::test]
async fn test_image() -> Result<()> {
    let client = test_client::testwp_client();
    let wikitext = "[[File:Example.jpg]]";
    let code = client.transform_to_html(wikitext).await?.into_mutable();
    let images = code.filter_images();
    assert_eq!(images.len(), 1);
    assert_eq!(images[0].title(), "File:Example.jpg");
    assert_eq!(
        images[0].horizontal_alignment(),
        HorizontalAlignment::Unspecified
    );
    images[0].set_horizontal_alignment(HorizontalAlignment::Center);
    let centered = client.transform_to_wikitext(&code).await?;
    assert_eq!(&centered, "[[File:Example.jpg|center]]");
    images[0].set_horizontal_alignment(HorizontalAlignment::Left);
    let left = client.transform_to_wikitext(&code).await?;
    assert_eq!(&left, "[[File:Example.jpg|left]]");
    images[0].set_horizontal_alignment(HorizontalAlignment::Unspecified);
    let unspecified = client.transform_to_wikitext(&code).await?;
    assert_eq!(&unspecified, wikitext);
    Ok(())
}

#[tokio::test]
async fn test_missing_image() -> Result<()> {
    let client = test_client::testwp_client();
    let code = client.get("Mwbot-rs/Gallery3").await?.into_mutable();
    let galleries: Vec<_> = code
        .inclusive_descendants()
        .filter_map(|node| node.as_gallery())
        .collect();
    let images = galleries[0].images();
    assert_eq!(images[0].title(), "File:ThisFileDoesNotExist.jpg");
    let errors = images[0].error().unwrap();
    assert_eq!(errors[0].key, "apierror-filedoesnotexist");
    Ok(())
}
