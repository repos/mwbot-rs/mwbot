/*
Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::prelude::*;

pub fn client(url: &str) -> ParsoidClient {
    ParsoidClient::new(url, "parsoid-rs testing").unwrap()
}

const TEST_ENDPOINT: &str = "https://{}/w/rest.php";
// const TEST_ENDPOINT: &str = "https://{}/api/rest_v1";
// const TEST_ENDPOINT: &str = "https://{}/w/rest.php/{}/v3";

pub fn mw_client() -> ParsoidClient {
    client(&TEST_ENDPOINT.replace("{}", "www.mediawiki.org"))
}

pub fn enwp_client() -> ParsoidClient {
    client(&TEST_ENDPOINT.replace("{}", "en.wikipedia.org"))
}

pub fn testwp_client() -> ParsoidClient {
    client(&TEST_ENDPOINT.replace("{}", "test.wikipedia.org"))
}
