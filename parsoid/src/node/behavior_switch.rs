/*
Copyright (C) 2024 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//! Behavior nodes represent a behavior switch (e.g. `__TOC__`)

use crate::assert_element;
use kuchikiki::{Attribute, ExpandedName, NodeRef};

/// Represents a behavior switch (e.g. `__TOC__`)
/// ```
/// # use parsoid::prelude::*;
/// // __TOC__
/// let switch = BehaviorSwitch::new("toc");
/// assert_eq!(
///     &switch.property(),
///     "toc"
/// );
/// assert_eq!(
///     &switch.to_string(),
///     "<meta property=\"mw:PageProp/toc\">"
/// );
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.8.0#Behavior_switches) for more details.
#[derive(Debug, Clone)]
pub struct BehaviorSwitch(pub(crate) NodeRef);

impl BehaviorSwitch {
    /// Create a new behavior switch
    pub fn new(property: &str) -> Self {
        let attributes = vec![(
            ExpandedName::new(ns!(), local_name!("property")),
            Attribute {
                prefix: None,
                value: format!("mw:PageProp/{property}"),
            },
        )];
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("meta")),
            attributes,
        );
        Self(element)
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get the property name
    pub fn property(&self) -> String {
        self.as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("property")
            .unwrap()
            .trim_start_matches("mw:PageProp/")
            .to_string()
    }
}
