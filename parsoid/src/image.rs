/*
Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//! Image-related code

use crate::{assert_element, clean_link, WikinodeIterator};
use kuchikiki::NodeRef;
use serde::Deserialize;
use urlencoding::decode;

/// Represents an image (`[[File:Foobar.jpg]]`)
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.8.0#Images) for more details.
#[derive(Debug, Clone)]
pub struct Image(pub(crate) NodeRef);

impl Image {
    // Could be mw:File, mw:File/Frameless, mw:File/Thumb, mw:File/Frame
    pub(crate) const TYPEOF_PREFIX: &'static str = "mw:File";
    pub(crate) const SELECTOR: &'static str = "[typeof^=\"mw:File\"]";

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get the errors, if any, that apply to this image. For example,
    /// if an image is missing.
    pub fn error(&self) -> Option<Vec<ImageError>> {
        let node = self.select_first("[typeof~=\"mw:Error\"]")?;
        let attrs = node.as_element()?.attributes.borrow();
        // XXX: Do we want better error handling here instead of silently
        // pretending there's no error?
        let data: DataMw = serde_json::from_str(attrs.get("data-mw")?).ok()?;
        Some(data.errors)
    }

    /// Get the MediaWiki page title corresponding to the image being embedded,
    /// including `File:` namespace prefix.
    pub fn title(&self) -> String {
        let node = self
            .select_first(".mw-file-element[resource]")
            .expect("Unable to find .mw-file-element[resource] node");
        let attrs = node.as_element().unwrap().attributes.borrow();
        clean_link(&decode(attrs.get("resource").unwrap()).unwrap())
    }

    /// Get the horizontal alignment of the image, see the [documentation](https://www.mediawiki.org/wiki/Help:Images#Horizontal_alignment)
    /// for more details.
    pub fn horizontal_alignment(&self) -> HorizontalAlignment {
        let attrs = self.as_element().unwrap().attributes.borrow();
        let class = attrs.get("class").unwrap_or("");
        for part in class.split(' ') {
            if part.starts_with("mw-halign-") {
                return HorizontalAlignment::from_class(part);
            }
        }

        // Not h-aligned
        HorizontalAlignment::Unspecified
    }

    /// Set the horizontal alignment of the image, see the [documentation](https://www.mediawiki.org/wiki/Help:Images#Horizontal_alignment)
    /// for more details.
    pub fn set_horizontal_alignment(&self, halign: HorizontalAlignment) {
        let mut class: Vec<_> = self
            .as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("class")
            .unwrap_or("")
            .split(' ')
            // Remove all existing mw-halign- classes
            .filter(|part| !part.starts_with("mw-halign-"))
            .map(|part| part.to_string())
            .collect();
        // Insert in our desired class
        if let Some(halign) = halign.as_class() {
            class.push(halign.to_string());
        }
        self.as_element()
            .unwrap()
            .attributes
            .borrow_mut()
            .insert("class", class.join(" ").trim().to_string());
    }
}

/// How the image should be horizontally aligned, see the [documentation](https://www.mediawiki.org/wiki/Help:Images#Horizontal_alignment)
/// for more details.
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum HorizontalAlignment {
    Left,
    Right,
    Center,
    None,
    Unspecified,
}

impl HorizontalAlignment {
    fn from_class(class: &str) -> Self {
        match class {
            "mw-halign-left" => Self::Left,
            "mw-halign-right" => Self::Right,
            "mw-halign-center" => Self::Center,
            "mw-halign-none" => Self::None,
            // TODO: Should we error out on an invalid halign class?
            _ => Self::Unspecified,
        }
    }

    fn as_class(&self) -> Option<&'static str> {
        match self {
            HorizontalAlignment::Left => Some("mw-halign-left"),
            HorizontalAlignment::Right => Some("mw-halign-right"),
            HorizontalAlignment::Center => Some("mw-halign-center"),
            HorizontalAlignment::None => Some("mw-halign-none"),
            HorizontalAlignment::Unspecified => None,
        }
    }
}

#[derive(Deserialize)]
struct DataMw {
    errors: Vec<ImageError>,
}

#[derive(Deserialize, Clone, Debug)]
pub struct ImageError {
    pub key: String,
    pub message: String,
}
