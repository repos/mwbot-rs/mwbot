/*
Copyright (C) 2020-2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::cite::{Reference, ReferenceLink, ReferenceList};
use crate::image::Image;
use crate::inclusion::{NoInclude, OnlyInclude};
use crate::node::{Category, Comment, ExtLink, Section, WikiLink};
use crate::private::Sealed;
use crate::template::{self, Template};
use crate::{attribute_contains_word, Result, WikiMultinode, Wikinode};
use kuchikiki::iter::{Ancestors, Descendants, Siblings};
use kuchikiki::NodeRef;
use std::iter::Rev;

/// Iterator wrapper to convert NodeRefs into Wikinodes
#[doc(hidden)]
pub struct WikinodeMap<I>(pub(crate) I);

impl<I> Iterator for WikinodeMap<I>
where
    I: Iterator<Item = NodeRef>,
{
    type Item = Wikinode;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next().map(|item| Wikinode::new_from_node(&item))
    }
}

/// Collection of iterators and mutators that allow operating on a tree of Wikinodes
pub trait WikinodeIterator: Sealed {
    fn as_node(&self) -> &NodeRef;

    /* Mutators */

    /// Append a node as a child
    fn append<N: WikiMultinode>(&self, code: &N) {
        for node in code.as_nodes() {
            self.as_node().append(node);
        }
    }

    /// Prepend a node as a child
    fn prepend<N: WikiMultinode>(&self, code: &N) {
        for node in code.as_nodes().iter().rev() {
            self.as_node().prepend(node.clone())
        }
    }

    /// Insert a node after the current node, as a sibling
    fn insert_after<N: WikiMultinode>(&self, code: &N) {
        for node in code.as_nodes().iter().rev() {
            self.as_node().insert_after(node.clone());
        }
    }

    /// Insert a node before the current node, as a sibling
    fn insert_before<N: WikiMultinode>(&self, code: &N) {
        for node in code.as_nodes() {
            self.as_node().insert_before(node);
        }
    }

    /* Selectors */

    /// Select some wiki nodes
    fn select(&self, selector: &str) -> Vec<Wikinode> {
        match self.as_node().select(selector) {
            Ok(select) => select
                .map(|node| Wikinode::new_from_node(node.as_node()))
                .collect(),
            Err(_) => vec![],
        }
    }

    /// Get the first element that matches the selector, if possible
    fn select_first(&self, selector: &str) -> Option<Wikinode> {
        match self.as_node().select_first(selector) {
            Ok(node) => Some(Wikinode::new_from_node(node.as_node())),
            Err(_) => None,
        }
    }

    /* Wiki iterators */
    /// Get a list of all wikilinks (`[[Foo|bar]]`)
    fn filter_links(&self) -> Vec<WikiLink> {
        self.select(WikiLink::SELECTOR)
            .iter()
            .map(|ref_| WikiLink::new_from_node(ref_.as_node()))
            .collect()
    }

    /// Get a list of all external links (`[https://example.org/ Example]`)
    fn filter_external_links(&self) -> Vec<ExtLink> {
        self.select(ExtLink::SELECTOR)
            .iter()
            .map(|ref_| ExtLink::new_from_node(ref_.as_node()))
            .collect()
    }

    /// Get a list of all categories
    fn filter_categories(&self) -> Vec<Category> {
        self.select(Category::SELECTOR)
            .iter()
            .map(|ref_| Category::new_from_node(ref_.as_node()))
            .collect()
    }

    /// Get a list of all comments (`<!-- example -->`)
    fn filter_comments(&self) -> Vec<Comment> {
        self.inclusive_descendants()
            .filter_map(|node| node.as_comment())
            .collect()
    }

    /// Get a list of all iamges
    fn filter_images(&self) -> Vec<Image> {
        self.select(Image::SELECTOR)
            .iter()
            .map(|ref_| Image::new_from_node(ref_.as_node()))
            .collect()
    }

    /// Get a list of templates
    fn filter_templates(&self) -> Result<Vec<Template>> {
        Ok(filter_templatelike(&self.select(Template::SELECTOR))?
            .into_iter()
            .filter(|temp| temp.is_template())
            .collect())
    }

    /// Get a list of parser functions.
    fn filter_parser_functions(&self) -> Result<Vec<Template>> {
        Ok(filter_templatelike(&self.select(Template::SELECTOR))?
            .into_iter()
            .filter(|temp| temp.is_parser_function())
            .collect())
    }

    /// Get a list of all reference links on the page, e.g. `[1]`.
    ///
    /// This includes both grouped and ungrouped references.
    fn filter_reference_links(&self) -> Vec<ReferenceLink> {
        self.select(ReferenceLink::SELECTOR)
            .iter()
            .map(|node| ReferenceLink::new_from_node(node.as_node()))
            .collect()
    }

    /// Get a list of all reference lists on the page, e.g. `<references>`
    fn filter_reference_lists(&self) -> Vec<ReferenceList> {
        self.select(ReferenceList::SELECTOR)
            .iter()
            .map(|node| ReferenceList::new_from_node(node.as_node()))
            .collect()
    }

    /// Get all references on the page.
    ///
    /// This contains grouped and ungrouped references, and are returned
    /// in the order they are presented in reference lists, so e.g. if the
    /// wikitext is:
    ///
    /// ```wikitext
    /// <references group="foo"/>
    /// <references/>
    /// ```
    ///
    /// then the "foo" group references will be ordered first in the list,
    /// and then the ungrouped ones.
    fn filter_references(&self) -> Vec<Reference> {
        self.filter_reference_lists()
            .iter()
            .flat_map(|list| list.references())
            .collect()
    }

    fn iter_sections(&self) -> Vec<Section> {
        self.select(Section::SELECTOR)
            .iter()
            .map(|node| Section::new_from_node(node.as_node()))
            .collect()
    }

    fn filter_noinclude(&self) -> Vec<NoInclude> {
        let mut start: Option<NodeRef> = None;
        let mut inner = vec![];
        let mut nodes = vec![];
        for node in self.inclusive_descendants() {
            if let Wikinode::Generic(node) = &node {
                if let Some(element) = node.as_element() {
                    if element.name.local == local_name!("meta") {
                        if let Some(typeof_) =
                            element.attributes.borrow().get("typeof")
                        {
                            if attribute_contains_word(
                                typeof_,
                                NoInclude::TYPEOF_START,
                            ) {
                                start = Some(node.as_node().clone());
                                inner = vec![];
                                continue;
                            } else if attribute_contains_word(
                                typeof_,
                                NoInclude::TYPEOF_END,
                            ) {
                                nodes.push(NoInclude::new_from_node(
                                    // TODO: a sanity check rather than unwrap here might be better.
                                    &start.clone().unwrap(),
                                    node.as_node(),
                                    &inner,
                                ));
                                continue;
                            }
                            // fall through
                        }
                    }
                }
            }
            if start.is_some() {
                inner.push(node.as_node().clone());
            }
        }

        nodes
    }

    fn filter_onlyinclude(&self) -> Vec<OnlyInclude> {
        let mut start: Option<NodeRef> = None;
        let mut inner = vec![];
        let mut nodes = vec![];
        for node in self.inclusive_descendants() {
            if let Wikinode::Generic(node) = &node {
                if let Some(element) = node.as_element() {
                    if element.name.local == local_name!("meta") {
                        if let Some(typeof_) =
                            element.attributes.borrow().get("typeof")
                        {
                            if attribute_contains_word(
                                typeof_,
                                OnlyInclude::TYPEOF_START,
                            ) {
                                start = Some(node.as_node().clone());
                                inner = vec![];
                                continue;
                            } else if attribute_contains_word(
                                typeof_,
                                OnlyInclude::TYPEOF_END,
                            ) {
                                nodes.push(OnlyInclude::new_from_node(
                                    // TODO: a sanity check rather than unwrap here might be better.
                                    &start.clone().unwrap(),
                                    node.as_node(),
                                    &inner,
                                ));
                                continue;
                            }
                            // fall through
                        }
                    }
                }
            }
            if start.is_some() {
                inner.push(node.as_node().clone());
            }
        }

        nodes
    }

    /* Iterators */

    /// Return the parent node, if it has one
    fn parent(&self) -> Option<Wikinode> {
        self.as_node()
            .parent()
            .map(|node| Wikinode::new_from_node(&node))
    }

    /// Return the next sibling node, if it has one
    fn next_sibling(&self) -> Option<Wikinode> {
        self.as_node()
            .next_sibling()
            .map(|node| Wikinode::new_from_node(&node))
    }

    /// Return the previous sibling node, if it has one
    fn previous_sibling(&self) -> Option<Wikinode> {
        self.as_node()
            .previous_sibling()
            .map(|node| Wikinode::new_from_node(&node))
    }

    /// Return an iterator of references to this node and its ancestors.
    fn inclusive_ancestors(&self) -> WikinodeMap<Ancestors> {
        WikinodeMap(self.as_node().inclusive_ancestors())
    }

    /// Return an iterator of references to this node’s ancestors.
    fn ancestors(&self) -> WikinodeMap<Ancestors> {
        WikinodeMap(self.as_node().ancestors())
    }

    /// Return an iterator of references to this node and the siblings before it.
    fn inclusive_preceding_siblings(&self) -> WikinodeMap<Rev<Siblings>> {
        WikinodeMap(self.as_node().inclusive_preceding_siblings())
    }

    /// Return an iterator of references to this node’s siblings before it.
    fn preceding_siblings(&self) -> WikinodeMap<Rev<Siblings>> {
        WikinodeMap(self.as_node().preceding_siblings())
    }

    /// Return an iterator of references to this node and the siblings after it.
    fn inclusive_following_siblings(&self) -> WikinodeMap<Siblings> {
        WikinodeMap(self.as_node().inclusive_following_siblings())
    }

    /// Return an iterator of references to this node’s siblings after it.
    fn following_siblings(&self) -> WikinodeMap<Siblings> {
        WikinodeMap(self.as_node().following_siblings())
    }

    /// Return an iterator of references to this node’s children.
    fn children(&self) -> WikinodeMap<Siblings> {
        WikinodeMap(self.as_node().children())
    }

    /// Return an iterator of references to this node and its descendants, in tree order.
    /// Parent nodes appear before the descendants.
    fn inclusive_descendants(&self) -> WikinodeMap<Descendants> {
        WikinodeMap(self.as_node().inclusive_descendants())
    }

    /// Return an iterator of references to this node’s descendants, in tree order.
    /// Parent nodes appear before the descendants.
    fn descendants(&self) -> WikinodeMap<Descendants> {
        WikinodeMap(self.as_node().descendants())
    }

    /* -- these don't work because Traverse's Item is not NodeRef
    /// Return an iterator of the start and end edges of this node and its descendants, in tree order.
    fn traverse_inclusive(&self) -> WikinodeMap<Traverse> {
        WikinodeMap(self.as_node().traverse_inclusive())
    }

    /// Return an iterator of the start and end edges of this node’s descendants, in tree order.
    fn traverse(&self) -> WikinodeMap<Traverse> {
        WikinodeMap(self.as_node().traverse())
    }
    */
}

/// Filters template-like things (actual templates and parser functions)
fn filter_templatelike(nodes: &[Wikinode]) -> Result<Vec<Template>> {
    let mut templates = vec![];
    for ref_ in nodes {
        let element = ref_.as_node();
        let data: template::Transclusion = serde_json::from_str(
            element
                .as_element()
                .unwrap()
                .attributes
                .borrow()
                .get("data-mw")
                .unwrap(),
        )?;
        // Identify siblings with the same about attribute
        let siblings = match element
            .as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("about")
        {
            Some(about) => {
                // TODO: do we need preceding_siblings?
                element
                    .following_siblings()
                    .filter(|node| {
                        if let Some(element) = node.as_element() {
                            if let Some(new_about) =
                                element.attributes.borrow().get("about")
                            {
                                return about == new_about;
                            }
                        }

                        false
                    })
                    .collect()
            }
            None => vec![],
        };

        for (part_num, part) in data.parts.iter().enumerate() {
            if let template::TransclusionPart::Template { template: _ } = part {
                templates.push(Template::new_from_node(
                    element, &siblings, part_num,
                ));
            }
            // Note: we ignore interspersed wikitext, and treat it as read-only,
            // which is the behavior documented in the spec.
        }
    }

    Ok(templates)
}
