/*
Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//! `<gallery>` tags allow for displaying a collection of media,
//! they are core functionality in MediaWiki but treated as an
//! extension in Parsoid.
//!
//! See the [specification](https://www.mediawiki.org/wiki/Specs/HTML/Extensions/Gallery) for more details.

use crate::image::Image;
use crate::{
    assert_element, inner_data, set_inner_data, Result, Wikinode,
    WikinodeIterator,
};
use indexmap::IndexMap;
use kuchikiki::{Attribute, ExpandedName, NodeRef};
use serde::{Deserialize, Serialize};

/// Represents a gallery tag (`<gallery>`)
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/Extensions/Gallery) for more details.
#[derive(Debug, Clone)]
pub struct Gallery(pub(crate) NodeRef);

impl Gallery {
    pub(crate) const TYPEOF: &'static str = "mw:Extension/gallery";
    const CAPTION_CLASS: &'static str = "gallerycaption";

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get a map of the attributes set on the `<gallery>` tag
    pub fn attributes(&self) -> Result<IndexMap<String, String>> {
        Ok(self.inner()?.attrs)
    }

    /// Set an attribute on the `<gallery>` tag. The previous value,
    /// if one was set, is returned.
    pub fn set_attribute(
        &self,
        name: &str,
        value: &str,
    ) -> Result<Option<String>> {
        let mut inner = self.inner()?;
        let old = inner.attrs.insert(name.to_string(), value.to_string());
        self.set_inner(inner)?;
        Ok(old)
    }

    /// Remove an attribute from the `<gallery>` tag. If set, the previous
    /// value is returned.
    pub fn remove_attribute(&self, name: &str) -> Result<Option<String>> {
        let mut inner = self.inner()?;
        let old = inner.attrs.swap_remove(name);
        self.set_inner(inner)?;
        Ok(old)
    }

    /// Images contained in this gallery. Note, does not include other
    /// media like audio or video files.
    pub fn images(&self) -> Vec<Image> {
        self.inclusive_descendants()
            .filter_map(|node| Wikinode::new_from_node(&node).as_image())
            .collect()
    }

    /// Get the gallery caption
    pub fn caption(&self) -> Option<String> {
        self.select_first(&format!(".{}", Self::CAPTION_CLASS))
            .map(|caption| caption.text_contents())
    }

    /// Set the gallery caption
    pub fn set_caption(&self, caption_text: String) -> Result<()> {
        // Remove the caption from the data attributes
        // This signals to Parsoid to serialize the caption from HTML
        // NOTE: This is no longer needed since native gallery editing is now the default
        self.remove_attribute("caption")?;

        let caption =
            match self.select_first(&format!(".{}", Self::CAPTION_CLASS)) {
                Some(caption) => {
                    for child in caption.children() {
                        child.detach();
                    }
                    caption
                }
                None => {
                    let li_node = NodeRef::new_element(
                        crate::build_qual_name(local_name!("li")),
                        vec![(
                            ExpandedName::new(ns!(), local_name!("class")),
                            Attribute {
                                prefix: None,
                                value: Self::CAPTION_CLASS.to_string(),
                            },
                        )],
                    );
                    let caption = Wikinode::new_from_node(&li_node);
                    self.prepend(&caption);
                    caption
                }
            };
        let text_node = NodeRef::new_text(caption_text);
        caption.append(&Wikinode::new_from_node(&text_node));
        Ok(())
    }

    fn inner(&self) -> Result<GalleryDataMw> {
        inner_data(self)
    }

    fn set_inner(&self, data: GalleryDataMw) -> Result<()> {
        set_inner_data(self, data)
    }
}

#[derive(Deserialize, Serialize)]
pub(crate) struct GalleryDataMw {
    pub(crate) name: String,
    pub(crate) attrs: IndexMap<String, String>,
}
