## 0.10.0-rc.4 / 2025-01-25
* Keep `data-parsoid` by default ([T382999](https://phabricator.wikimedia.org/T382999))
* Retry on HTTP 429 errors ([T383000](https://phabricator.wikimedia.org/T383000))
* Migrate most tests to use test.wikipedia.org
* Use `flavor=edit` from fetching Parsoid HTML in the core API

## 0.10.0-rc.3 / 2024-11-23
* `WikiLink::target()` now replaces `_` in URL fragments to spaces as well.
* `WikiLink::target()` now propely parses links with %-encoded characters.
* Improved documentation with more internal links.
* Add support for `<ref>` and `<references/>` tags.

## 0.10.0-rc.2 / 2024-06-13
* Remove deprecated `WikinodeIterator::preceding_simblings()`; use the
  correctly spelled `preceding_siblings()` instead.
* Raise MSRV to 1.74.

## 0.10.0-rc.1 / 2024-06-03
This release properly supports the MediaWiki REST API. Restbase,
which was really used only on Wikimedia sites, is now deprecated.

URLs should now be in the form of `https://example.org/w/rest.php`.

For example, the English Wikipedia is `https://en.wikipedia.org/w/rest.php`.

Other changes include:
* Add `WikiLink::is_redirect()`
* Strip all `data-parsoid` attributes, which are only intended for
  internal usage.

## 0.9.1 / 2024-01-23
* Fix `Image.title()` for missing images, expose error information.
* Fix `<indicator>` support.

## 0.9.0 / 2023-12-15
* [BREAKING] Switch to maintained `kuchikiki` fork for HTML parsing library.
* Fix syntax error in documentation

## 0.9.0-rc.1 / 2023-11-02
* Increase MSRV to 1.67.
* [BREAKING] Upgrade `indexmap` dependency to 2.0.
* [BREAKING] Request [version 2.8.0](https://www.mediawiki.org/wiki/Specs/HTML/2.8.0) of HTML
  from MediaWiki.
  * `DISPLAYTITLE` and `DEFAULTSORT` are now templates (as parser functions) instead of behavior switches
  * `BehaviorSwitch` no longer supports having an associated `content()`.
* Make it possible to get Wikinodes from Multinodes
* Fix typo in `preceding_siblings()` iterator function.


## 0.8.0 / 2023-04-02
* Increase MSRV to 1.63.
* Request [version 2.7.0](https://www.mediawiki.org/wiki/Specs/HTML/2.7.0) of HTML from MediaWiki.
* [BREAKING] Remove deprecated `Wikicode.set_tag()` and `Wikicode.etag()` methods.
* Add accessor and mutator for an image's horizontal alignment.
* [BREAKING] `parsoid::Result` is no longer included in the `prelude`.
* Add mutators for attributes on the `<gallery>` tag.
* [BREAKING] `Gallery.wikitext()` and `Gallery.set_wikitext()` were removed, MediaWiki no longer
  supports them.
* Add method to set a gallery's caption.

## 0.8.0-alpha.5 / 2023-02-23
* Handle "templatearg" parts in a multi-part template instead of erroring. Fixes [T330371](https://phabricator.wikimedia.org/T330371).

## 0.8.0-alpha.4 / 2023-02-20
* [BREAKING] Use own `Error` type instead of `mwapi_errors`.
* Add helper functions to detect if WikiLinks or ExtLinks are [magic links](https://www.mediawiki.org/wiki/Help:Magic_links).
* Fix detection of interwiki links.
* [BREAKING] `Template::remove_param()`, `set_param()`, `set_params()` now
  return the previous value(s), if set.

## 0.8.0-alpha.3 / 2023-02-07
* Add `Template::set_name()` to change the name of an existing template.
* Add `Template::set_params()` to set multiple parameters at once.

## 0.8.0-alpha.2 / 2023-02-01
* Improve rendering of documentation on docs.rs.

## 0.8.0-alpha.1 / 2023-01-30
* [BREAKING] Upgrade `mwapi_errors` to 0.3.0-alpha.1

## 0.7.5 / 2023-01-08
* Fix `.title()` for pages that have a custom DISPLAYTITLE. This also fixes functions
  that used it, including `.transform_to_wikitext()`. Fixes [T326490](https://phabricator.wikimedia.org/T326490).
* Deprecate `.etag()` and `.set_etag()` as internal implementation details that shouldn't
  have been public in the first place. Please reach out if you'd like these functions to remain.

## 0.7.4 / 2022-12-04
* Fix percent encoding of unicode characters in links, fixing a regression from 0.7.3.

## 0.7.3 / 2022-12-01 (yanked)
* Fix parsing of link target when the link doesn't exist (a redlink), following a Parsoid change.
  Fixes [T324129](https://phabricator.wikimedia.org/T324129).

## 0.7.2 / 2022-11-07
* Add support for the `wasm32` architecture.

## 0.7.1 / 2022-10-01
* Add `.parent()`, `.next_sibling()`, `.previous_sibling()` node operators.
* Git repository moved to [Wikimedia GitLab](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot).
* Issue tracking moved to [Wikimedia Phabricator](https://phabricator.wikimedia.org/project/profile/6182/).

## 0.7.0 / 2022-09-19
* Fix detection of external links following a [change in Parsoid](https://phabricator.wikimedia.org/T186241).
* Increase MSRV to 1.60.

## 0.7.0-alpha.5 / 2022-09-11
* Fix images with characters that get URL encoded
* Bump MSRV to 1.59

## 0.7.0-alpha.4 / 2022-05-01
* [BREAKING] Use `u64` for all revision ID fields. Fixes [#56](https://gitlab.com/mwbot-rs/mwbot/-/issues/56).
* Add very basic support for reading images and galleries.

## 0.7.0-alpha.3 / 2022-01-04
* [BREAKING] Update mwapi_errors to 0.2.0-alpha.1

## 0.7.0-alpha.2 / 2021-12-25
* Export `ImmutableWikicode` from crate root.

## 0.7.0-alpha.1 / 2021-12-24
* [BREAKING] Require Rust 1.56.
* Move Git repository to <https://gitlab.com/mwbot-rs/mwbot>
* Relicense as GPL v3 or later, for compatibility with rest of `mwbot-rs`.
* [BREAKING] Client now returns `ImmutableWikicode` by default. This should
  avoid a lot of issues related to `Wikicode` not being `Send`. Fixes [#40](https://gitlab.com/mwbot-rs/mwbot/-/issues/40).
* [BREAKING] All public traits are now sealed.
* [BREAKING] Drop "get_" prefix from most functions per Rust API guidelines.
* [BREAKING] Rename most "to_" functions to "into_" per Rust API guidelines.

## 0.6.4 / 2021-11-21
* Have `ImmutableWikicode` implement `Debug` and `Clone`.

## 0.6.3 / 2021-11-19
* Limit `Section` to those with a `data-mw-section-id` attribute. This fixes a
  panic in `Section::section_id()`. Fixes [#14](https://gitlab.com/mwbot-rs/parsoid/-/issues/14).
* Add convenient `to_mutable()` and `to_immutable()` functions on
  `ImmutableWikicode` and `Wikicode` respectively.

## 0.6.2 / 2021-11-13
* Don't automatically follow redirects, as originally intended. Clients need to
  explicitly follow redirects by checking `get_redirect()`.
* Vary concurrency limit based on rest.php or Restbase. For rest.php the
  concurrency limit is 1, while for Restbase it's 100.

## 0.6.1 / 2021-11-05
* Update to [HTML spec 2.3.0](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0),
  no practical changes for us.
* Add `Wikicode::spec_version()` to get the HTML spec version, if available.
* Rename `Wikicode::title()` to `get_title()`. The old name is still available
  but deprecated.
* Introduce `ImmutableWikicode`, which allows for using `Wikicode` in a
  thread-safe manner.
* Git repository has moved to [https://gitlab.com/mwbot-rs/parsoid](https://gitlab.com/mwbot-rs/parsoid).
* Use `tracing` instead of `log` for logging HTTP requests. Fixes [#11](https://gitlab.com/mwbot-rs/parsoid/-/issues/11).
* Allow using an existing `reqwest::Client` instance when creating a new
  `ParsoidClient`. This will allow sharing connection pools and cookie state
  with other crates making API requests. Fixes [#7](https://gitlab.com/mwbot-rs/parsoid/-/issues/7).

## 0.6.0 / 2021-11-01
* [BREAKING] `Template` no longer has *_on functions for adding it to other
  Wikinodes. Instead, the standard append/prepend/insert_after/insert_before
  functions all support regular Wikinodes and "multi nodes", which include
  templates, noinclude and onlyinclude.
* Document safety expectations and limitations
* Implement `<onlyinclude>` tag support. Document that `<noinclude>` support
  is no longer preliminary.
* Implement support for DisplaySpace nodes, which are added as a
  post-processing step by Parsoid.
* Implement support for Placeholder nodes, which should not be edited by the
  client.
* Implement `<indicator>` tag support.
* Optimize `filter_templates` and `filter_parser_functions`.

## 0.6.0-alpha.3 / 2021-10-20
* The HTTP `Client` now implements `Debug` and `Clone`.
* Client.transform_to_wikitext_raw() now optionally takes a title,
  revision id, and etag, just as transform_to_wikitext() uses.

## 0.6.0-alpha.2 / 2021-10-18
* Implement `<includeonly>` tag support
* Implement preliminary `<noinclude>` tag support
* `filter_comments` is now supported on any Wikinode
* Use [`mwapi_errors`](https://docs.rs/mwapi_errors/0.1) crate for `Error`

## 0.6.0-alpha.1 / 2021-10-13
* Prelude now imports HTTP client as `ParsoidClient` to avoid conflicts with
  other things named `Client`.
* Implement basic concurrency limiting for requests; currently hardcoded to
  10 concurrent requests.

## 0.5.1 / 2021-10-02
* Fix Template.name() for templates with ? in them.
* Improve documentation, including lack of thread-safety
  and internal details about templates' data-mw.
* Switch to using rustls for TLS.
* Update `urlencoding` dependency to 2.1.

## 0.5.0 / 2020-02-20
* [BREAKING] `filter_comments()` now returns `Vec<Comment>`.
* [BREAKING] `Wikicode::new_fragment()` has been replaced by
  `Wikicode::new_node()` and `Wikicode::new_text()`
* [BREAKING] `Heading::new()` takes `&NodeRef` as the second parameter instead
  of a `&str`.
* [BREAKING] `filter_templates()` will only return templates. For parser
  functions use `filter_parser_functions()`.
* [BREAKING] Rename most target/link functions guide people into the
  "pretty" formats by default, providing `raw_` functions if you need it.
* Various link functions take `&NodeRef` as the text parameter.
* Add `Client::get_revision()` to fetch old revisions.
* Mark `Error` as `#[non_exhaustive]`.
* Add request logging using `log`.

## 0.5.0-alpha.1 / 2021-02-11
* [BREAKING] `Template` now represents a list of sibling nodes instead of just one
  * To access the underlying nodes, use `temp.as_nodes()` instead of the now
    removed `temp.get_node()`. Helpers for most `.attach()`, `.prepend()`, etc.
    operations are available in `Template`.
* [BREAKING] `filter_links()`, `filter_external_links()` now return
  `Vec<Wikinode>`.
* Update to [HTML spec 2.2.0](https://www.mediawiki.org/wiki/Specs/HTML/2.2.0),
  no practical changes for us.
* Make `Wikinode::` ... `prepend()`, `insert_before()`, `insert_after()` usable
* Add `Template::new_simple()` for creation without parameters

## 0.4.2 / 2021-04-07
* Add helpers to get a "pretty" page title for links and templates

## 0.4.1 / 2021-04-06
* Parsoid's `Error` is now available as `parsoid::Error`
* Add specific `Error::PageDoesNotExist` type
* Add specific `Error::InvalidEtag` type

## 0.4.0 / 2021-04-05

* Use dependencies that use Tokio 1.0
* Fix roundtripping of some Cite errors

## 0.3.0-alpha.3 / 2020-11-11
* First usable release
