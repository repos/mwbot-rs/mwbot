## 0.2.5 / 2024-09-04
* Add `NamespaceMap::all_namespaces()`
* Raise MSRV to 1.74.

## 0.2.4 / 2024-06-02
* Add `TitleCodec::namespace_map()`
* Make `Namespace` type public
* Raise MSRV to 1.73.

## 0.2.3 / 2023-12-15
* Increase MSRV to 1.67.
* Fix `partial_cmp`/`cmp` implementations, per clippy
* Add `NS_MAIN`, `NS_TALK`, etc. constants for MediaWiki's default namespace numbers.

## 0.2.2 / 2023-02-20
* Add function to create title from numerical namespace ID

## 0.2.1 / 2022-10-01
* Git repository moved to [Wikimedia GitLab](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot).
* Issue tracking moved to [Wikimedia Phabricator](https://phabricator.wikimedia.org/project/profile/6182/).

## 0.2.0 / 2022-09-19
* Officially declare MSRV as 1.60.

## 0.2.0-alpha.2 / 2022-05-01
* [BREAKING] `TitleCodec.namespace_map` is no longer public.
* `NamespaceMap::display_title` is now public.
* [BREAKING] `Title::new_unchecked` is now `unsafe`, as it allows breaking
  assumptions about the namespace/dbkey fields.
* [BREAKING] `Title::new_unchecked` and `Title::with_fragment` take a `String`
  to reduce allocations when possible.

## 0.2.0-alpha.1 / 2022-01-06
* [BREAKING] Improve documentation, add feature tags.
  * `SiteInfo` fields and types are exported in the crate root directly rather
    than in a `site_info` module.
* [BREAKING] Error is now clonable, upstream errors are now wrapped in
  `Arc<T>`.

## 0.1.0 / 2021-12-19
* Initial release
