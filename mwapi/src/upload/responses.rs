use serde::Deserialize;
use serde_json::Value;
use std::collections::HashMap;

#[derive(Deserialize, Debug)]
pub(crate) struct SimpleResponse {
    pub(crate) upload: SimpleUploadResponse,
}

#[derive(Deserialize, Debug)]
#[serde(tag = "result")]
pub(crate) enum SimpleUploadResponse {
    Warning { warnings: HashMap<String, Value> },
    Success { filename: String },
}
