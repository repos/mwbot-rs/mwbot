/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! A MediaWiki API client library.
//!
//! `mwapi` is a low-level library for the [MediaWiki Action API](https://www.mediawiki.org/wiki/API:Main_page).
//! If you intend to edit pages or want a higher-level interface, it's recommended to use [`mwbot`](https://docs.rs/mwbot/),
//! which builds on top of this crate.
//!
//! ## Goals
//! * generic to fit any application, whether for interactive usage
//!   or writing a bot
//! * fully compatible with concurrent use cases
//! * turns MediaWiki errors into Rust errors for you
//! * logging (using the `tracing` crate) for visiblity into errors
//! * follow all [best practices](https://www.mediawiki.org/wiki/API:Etiquette)
//!
//! ## Quick start
//! ```
//! # #[tokio::main]
//! # async fn main() -> mwapi::Result<()> {
//! let client = mwapi::Client::builder("https://en.wikipedia.org/w/api.php")
//!     .set_user_agent("mwapi demo")
//!     // Provide credentials for login:
//!     // .set_botpassword("username", "password")
//!     .build().await?;
//! let resp = client.get_value(&[
//!     ("action", "query"),
//!     ("prop", "info"),
//!     ("titles", "Taylor Swift"),
//! ]).await?;
//! let info = resp["query"]["pages"][0].clone();
//! assert_eq!(info["ns"].as_u64().unwrap(), 0);
//! assert_eq!(info["title"].as_str().unwrap(), "Taylor Swift");
//! # Ok(())
//! # }
//!
//! ```
//!
//! ## Functionality
//! * authentication, using [OAuth2](https://www.mediawiki.org/wiki/OAuth/Owner-only_consumers#OAuth_2) (recommended) or BotPasswords
//! * error handling, transforming MediaWiki errors into Rust ones
//! * CSRF [token](https://www.mediawiki.org/wiki/API:Tokens) handling with `post_with_token`
//! * rate limiting and concurrency controls
//! * file uploads (needs `upload` feature)
//!
//! ## See also
//! * [`mwbot`](https://docs.rs/mwbot/) provides a higher level interface to
//!   interacting with MediaWiki
//! * [`mwapi_responses`](https://docs.rs/mwapi_responses/) is a macro to
//!   generate strict types for dynamic API queries
//!
//! ## Contributing
//! `mwapi` is a part of the [`mwbot-rs` project](https://www.mediawiki.org/wiki/Mwbot-rs).
//! We're always looking for new contributors, please [reach out](https://www.mediawiki.org/wiki/Mwbot-rs#Contributing)
//! if you're interested!
#![deny(clippy::all)]
#![deny(rustdoc::all)]
#![cfg_attr(docsrs, feature(doc_cfg))]

mod client;
mod error;
mod params;
mod responses;
mod time;
mod tokens;
#[cfg(feature = "upload")]
mod upload;

const VERSION: &str = env!("CARGO_PKG_VERSION");

pub use client::{Builder, Client};
pub use error::{ApiError, Error};
pub use params::Params;
use std::fmt::{Display, Formatter};

pub type Result<T> = std::result::Result<T, Error>;

/// Assert that your account has the specified login state, see
/// [API:Assert](https://www.mediawiki.org/wiki/API:Assert) for more details.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Assert {
    /// Not logged in, aka anonymous
    Anonymous,
    /// Logged in to a bot account
    Bot,
    /// Logged in (to any account)
    User,
    /// Do not add any assert
    None,
}

impl Assert {
    fn value(&self) -> Option<&'static str> {
        match self {
            Assert::Anonymous => Some("anon"),
            Assert::Bot => Some("bot"),
            Assert::User => Some("user"),
            Assert::None => None,
        }
    }
}

impl Default for Assert {
    fn default() -> Self {
        Self::None
    }
}

#[derive(Clone, Copy, Debug)]
pub enum ErrorFormat {
    Html,
    Wikitext,
    Plaintext,
}

impl Display for ErrorFormat {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let val = match &self {
            ErrorFormat::Html => "html",
            ErrorFormat::Wikitext => "wikitext",
            ErrorFormat::Plaintext => "plaintext",
        };
        write!(f, "{val}")
    }
}

impl Default for ErrorFormat {
    fn default() -> Self {
        Self::Plaintext
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub(crate) enum Method {
    Get,
    Post,
}
